# Getting Started

## CARTRIDGE OVERVIEW

 The objective of the Buy Now Functionality Cartridge for Salesforce Commerce Cloud aims to enhance the e-commerce experience by introducing a streamlined and efficient "Buy Now" feature. This feature allows customers to make quick purchases with minimal steps, catering to users who prefer a fast and convenient shopping process. Through the integration with the existing SFCC cart and checkout processes and user-friendly interfaces, this cartridge seeks to elevate customer satisfaction, boost sales, and confer a competitive advantage in the continually evolving e-commerce arena.

 ## KEY FEATURES

* Buy Now Button on PDP & PLP: The "Buy Now" button on the PDP and PLP allows you to initiate a purchase with the selected product variant and quantity.
* Bypass Cart to Checkout: The "Buy Now" button on the PDP and PLP will bypass the cart page.
* Retain existing products in the Cart: When a user opt to initiate the Buy Now process for a single product while having other items in their cart, the existing Cart objects will be retained.
* Buy Now identifier in Order Object: We have a custom attribute to the Order object, specifically designed to indicate whether the order was initiated through the Buy Now option.

## INSTALLATION GUIDE

* Clone the cartridge into your project folder

* Change directory to the cartridge  folder and install node module depedencies in cartridge directory

    ```Shell
    cd plugin_buynow

    npm install

* Run “npm run build” to compile js & css

* Upload the cartridge to your active code version

* Add plugin_buynow to the site cartridge path under Administration > Sites > Manage Sites > <Site ID> - Settings

* Import the metadata provided in the “metadata” folder in the root directory of the cartridge.

## HOW TO USE

Once you have completed the cartridge installation and meta import, you have to update the custom preference to enable the buy now button.

### Custom Preference Setup

* Go to Merchant Tools > Site Preferences > Custom Site Preference Groups > Buy Now

* Here you have to update the custom preference to enable the Buy Now feature at global level

* Then Go to Merchant Tools > Products and Catalogs > Products > <Product ID> - General

* Here you have to update the custom preference to enable the Buy Now feature at product level.

### Reporting

* Go to Merchant Tools > Ordering > Orders

* Go to the Advanced tab in filters.

* Here you can filter orders using isBuyNow custom attribute in extended attributes.

## Change logs

* v1.0.0&emsp;|&emsp;2024-03-22&emsp;|&emsp;first release!

## About Author
- My name is Vikas Bhakhar, and I'm a Salesforce Commerce Cloud developer with 3 years of experience in building e-commerce solutions with overall experience of 10 years. I'm passionate about streamlining processes and creating user-friendly tools. Feel free to connect with me on [LinkedIn](https://www.linkedin.com/in/vikas-bhakhar-3b8bb3b7/).