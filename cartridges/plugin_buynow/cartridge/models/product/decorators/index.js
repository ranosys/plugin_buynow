'use strict';

var base = module.superModule;

base.buynow = require('*/cartridge/models/product/decorators/buynow');

module.exports = base;
