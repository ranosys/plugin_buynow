'use strict';

module.exports = function (object, apiProduct) {
    Object.defineProperty(object, 'enableBuyNow', {
        enumerable: true,
        value: apiProduct && 'enableBuyNow' in apiProduct.custom ? apiProduct.custom.enableBuyNow : false
    });
};
