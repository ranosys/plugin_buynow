'use strict';

/**
 * appends params to a url
 * @param {string} data - data returned from the server's ajax call
 * @param {Object} button - button that was clicked to buy now a product
 */
function displayMessage(data, button) {
    $.spinner().stop();
    var status;
    if (!data.error) {
        if (data.redirectUrl) {
            window.location.href = data.redirectUrl;
        }
        return;
    }
    status = 'alert-danger';

    if ($('.buynow-messages').length === 0) {
        $('body').append('<div class="buynow-messages"></div>');
    }
    $('.buynow-messages')
        .append('<div class="buynow-alert text-center ' + status + '">' + data.msg + '</div>');

    setTimeout(function () {
        $('.buynow-messages').remove();
        button.removeAttr('disabled');
    }, 5000);
}

/**
 * Retrieves the relevant pid value
 * @param {jquery} $el - DOM container for a given add to cart button
 * @return {string} - value to be used when adding product to cart
 */
function getPidValue($el) {
    var pid;

    if ($('#quickViewModal').hasClass('show') && !$('.product-set').length) {
        pid = $($el).closest('.modal-content').find('.product-quickview').data('pid');
    } else if ($('.product-set-detail').length || $('.product-set').length) {
        pid = $($el).closest('.product-detail').find('.product-id').text();
    } else if ($($el).closest('.product').length) {
        pid = $($el).closest('.product').data('pid');
    } else {
        pid = $('.product-detail:not(".bundle-item")').data('pid');
    }

    return pid;
}

/**
 * Retrieve product options
 *
 * @param {jQuery} $productContainer - DOM element for current product
 * @return {string} - Product options and their selected values
 */
function getOptions($productContainer) {
    var options = $productContainer
        .find('.product-option')
        .map(function () {
            var $elOption = $(this).find('.options-select');
            var urlValue = $elOption.val();
            var selectedValueId = $elOption.find('option[value="' + urlValue + '"]')
                .data('value-id');
            return {
                optionId: $(this).data('option-id'),
                selectedValueId: selectedValueId
            };
        }).toArray();

    return JSON.stringify(options);
}

/**
 * Retrieves the bundle product item ID's for the Controller to replace bundle master product
 * items with their selected variants
 *
 * @return {string[]} - List of selected bundle product item ID's
 */
function getChildProducts() {
    var childProducts = [];
    $('.bundle-item').each(function () {
        childProducts.push({
            pid: $(this).find('.product-id').text(),
            quantity: parseInt($(this).find('label.quantity').data('quantity'), 10)
        });
    });

    return childProducts.length ? JSON.stringify(childProducts) : [];
}

/**
 * Retrieve contextual quantity selector
 * @param {jquery} $el - DOM container for the relevant quantity
 * @return {jquery} - quantity selector DOM container
 */
function getQuantitySelector($el) {
    var quantitySelected;
    if ($el && $('.set-items').length) {
        quantitySelected = $($el).closest('.product-detail').find('.quantity-select');
    } else if ($el && $('.product-bundle').length) {
        var quantitySelectedModal = $($el).closest('.modal-footer').find('.quantity-select');
        var quantitySelectedPDP = $($el).closest('.bundle-footer').find('.quantity-select');
        if (quantitySelectedModal.val() === undefined) {
            quantitySelected = quantitySelectedPDP;
        } else {
            quantitySelected = quantitySelectedModal;
        }
    } else {
        quantitySelected = $('.quantity-select') || 1;
    }
    return quantitySelected;
}

/**
 * Retrieves the value associated with the Quantity pull-down menu
 * @param {jquery} $el - DOM container for the relevant quantity
 * @return {string} - value found in the quantity input
 */
function getQuantitySelected($el) {
    return getQuantitySelector($el).val();
}


// /**
//  * Makes a call to the server to report the event of adding an item to the cart
//  *
//  * @param {Object} data - a string representing the end point to hit so that the event can be recorded, or false
//  */
// function miniCartReportingUrl(data, button) {
//     if (data.reportingURL) {
//         $.ajax({
//             url: data.reportingURL,
//             method: 'GET',
//             success: function () {
//                 // reporting urls hit on the server
//                 displayMessage(data, button);
//             },
//             error: function () {
//                 // no reporting urls hit on the server
//                 displayMessage(data, button);
//             }
//         });
//     }
// }

var isCheckoutPageOpen = false;
var channel = new BroadcastChannel('myChannel');
channel.onmessage = function (event) {
    if (event.data === 'checkout') {
        isCheckoutPageOpen = true;
    }
    var action = $('[name="buynow-current-action"]').val();
    if (['Checkout-'].indexOf(action.split('-')[0] + '-') !== -1) {
        channel.postMessage('checkout');
    }
};
channel.postMessage('ping');

module.exports = {
    buynow: function () {
        $('body').on('click', '.pdp-buy-now', function () {
            var addToCartUrl = $(this).data('href');
            var pid;
            var pidsObj;
            var setPids;
            var button = $(this);

            $('body').trigger('product:beforeAddToCart', this);

            if ($('.set-items').length && $(this).hasClass('buy-now-global')) {
                setPids = [];

                $('.product-detail').each(function () {
                    if (!$(this).hasClass('product-set-detail')) {
                        setPids.push({
                            pid: $(this).find('.product-id').text(),
                            qty: $(this).find('.quantity-select').val(),
                            options: getOptions($(this))
                        });
                    }
                });
                pidsObj = JSON.stringify(setPids);
            }

            pid = getPidValue($(this));

            var $productContainer = $(this).closest('.product-detail');
            if (!$productContainer.length) {
                $productContainer = $(this).closest('.quick-view-dialog').find('.product-detail');
            }

            var form = {
                pid: pid,
                pidsObj: pidsObj,
                childProducts: getChildProducts(),
                quantity: getQuantitySelected($(this))
            };

            if (!$('.bundle-item').length) {
                form.options = getOptions($productContainer);
            }

            $(this).trigger('updateAddToCartFormData', form);
            if (addToCartUrl) {
                $.ajax({
                    url: addToCartUrl,
                    method: 'POST',
                    data: form,
                    success: function (data) {
                        $.spinner().stop();
                        // miniCartReportingUrl(data, button);
                        displayMessage(data, button);
                    },
                    error: function (err) {
                        $.spinner().stop();
                        displayMessage(err, button);
                    }
                });
            }
        });
    },
    updateCart: function () {
        var action = $('[name="buynow-current-action"]').val();
        var url = $('[name="buynow-update-action"]').val();
        setTimeout(function () {
            if (!isCheckoutPageOpen && ['Checkout-'].indexOf(action.split('-')[0] + '-') === -1) {
                $.ajax({
                    url: url,
                    type: 'post',
                    dataType: 'json',
                    success: function (data) {
                        $('.minicart').trigger('count:update', data);
                        if (data.refreshCart && ['Cart-Show'].indexOf(action) !== -1) {
                            window.location.reload();
                        }
                    }
                });
            }
        }, 500);
    },
    updateBuynow: function () {
        $('body').on('product:updateAddToCart', function (e, response) {
            if ($('#quickViewModal').hasClass('show') && !$('.product-set').length) {
                $('button.pdp-buy-now', $('#quickViewModal')).attr(
                    'disabled',
                    (!response.product.readyToOrder || !response.product.available)
                );
            } else {
                $('button.pdp-buy-now', response.$productContainer).attr(
                    'disabled',
                    (!response.product.readyToOrder || !response.product.available)
                );
            }
        });
    }
};
