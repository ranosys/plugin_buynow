'use strict';

/**
 * add current basket items to the user profile
 * @param {Object} req - The request object for the current controller request
 * @param {Object} res - The response object for the current controller request
 * @param {Function} next - Executes the next step in the controller chain
 */
function addCartItemsToProfile(req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var Transaction = require('dw/system/Transaction');
    var currentBasket = BasketMgr.getCurrentOrNewBasket();
    if (currentBasket && req.currentCustomer.raw.isAuthenticated()) {
        var cart = {};
        currentBasket.productLineItems.toArray().forEach(function (pli) {
            cart[pli.productID] = pli.quantity;
        });
        Transaction.wrap(function () {
            req.currentCustomer.raw.profile.custom.savedCart = JSON.stringify(cart);
        });
    }
    next();
}

/**
 * add saved cart items to the current basket
 * @param {Object} cart - cart that needs to be added
 */
function updateCart(cart) {
    var BasketMgr = require('dw/order/BasketMgr');
    var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
    var currentBasket = BasketMgr.getCurrentOrNewBasket();
    if (currentBasket) {
        Object.keys(cart).forEach(function (item) {
            cartHelper.addProductToCart(
                currentBasket,
                item,
                cart[item],
                [],
                []
            );
        });
        cartHelper.ensureAllShipmentsHaveMethods(currentBasket);
        basketCalculationHelpers.calculateTotals(currentBasket);
    }
}


module.exports = {
    updateCart: updateCart,
    addCartItemsToProfile: addCartItemsToProfile
};
