'use strict';

var base = module.superModule;

/**
 * Attempts to create an order from the current basket
 * @param {dw.order.Basket} currentBasket - The current basket
 * @returns {dw.order.Order} The order object created from the current basket
 */
function createOrder(currentBasket) {
    var Transaction = require('dw/system/Transaction');
    var buynowHelpers = require('*/cartridge/scripts/helpers/buynowHelpers');
    var isBuynow = 'isBuynow' in currentBasket.custom ? currentBasket.custom.isBuynow : null;
    var basketCart = 'savedCart' in currentBasket.custom ? JSON.parse(currentBasket.custom.savedCart || '{}') : {};
    var order = base.createOrder.apply(base, arguments);

    try {
        if (order) {
            Transaction.wrap(function () {
                order.custom.isBuynow = isBuynow;
                if (basketCart && Object.keys(basketCart).length) {
                    buynowHelpers.updateCart(basketCart);
                    if (session.isCustomerAuthenticated()) {
                        session.getCustomer().profile.custom.savedCart = null;
                    }
                } else if (session.isCustomerAuthenticated()) {
                    var profile = session.getCustomer().profile;
                    if (!isBuynow) {
                        profile.custom.savedCart = null;
                    }
                    var profileCart = JSON.parse(('savedCart' in profile.custom && profile.custom.savedCart) || '{}');
                    if (profileCart && Object.keys(profileCart).length) {
                        buynowHelpers.updateCart(profileCart);
                    }
                }
            });
        }
    } catch (error) {
        // handle error
    }

    return order;
}

module.exports = {
    createOrder: createOrder
};

Object.keys(base).forEach(function (prop) {
    // eslint-disable-next-line no-prototype-builtins
    if (!module.exports.hasOwnProperty(prop)) {
        module.exports[prop] = base[prop];
    }
});
