'use strict';

var server = require('server');
server.extend(module.superModule);

var buynowHelpers = require('*/cartridge/scripts/helpers/buynowHelpers');

server.append('AddProduct', buynowHelpers.addCartItemsToProfile);
server.append('RemoveProductLineItem', buynowHelpers.addCartItemsToProfile);
server.append('UpdateQuantity', buynowHelpers.addCartItemsToProfile);

module.exports = server.exports();
