'use strict';

/**
 * @namespace BuyNow
 */

var server = require('server');

/**
 * BuyNow-AddProduct : The BuyNow-Product endpoint is responsible for adding product to the cart and redirects user to the checkout page
 * @name Base/BuyNow-Product
 * @function
 * @memberof BuyNow
 * @param {httpparameter} - pid - product ID
 * @param {httpparameter} - quantity - quantity of product
 * @param {httpparameter} - options - list of product options
 * @param {returns} - json
 * @param {serverfunction} - post
 */
server.post('AddProduct', function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var Transaction = require('dw/system/Transaction');
    var URLUtils = require('dw/web/URLUtils');
    var Resource = require('dw/web/Resource');
    var cartHelper = require('*/cartridge/scripts/cart/cartHelpers');
    var basketCalculationHelpers = require('*/cartridge/scripts/helpers/basketCalculationHelpers');
    var currentBasket = BasketMgr.getCurrentOrNewBasket();
    var productId = req.form.pid;
    var childProducts = Object.hasOwnProperty.call(req.form, 'childProducts')
        ? JSON.parse(req.form.childProducts)
        : [];
    var options = req.form.options ? JSON.parse(req.form.options) : [];
    var quantity;
    var result;
    var pidsObj;
    var savedCart = {};

    if (currentBasket) {
        Transaction.wrap(function () {
            // Clear any existing line items out of the basket
            currentBasket.productLineItems.toArray().forEach(function (pli) {
                savedCart[pli.productID] = pli.quantity.value;
                currentBasket.removeProductLineItem(pli);
            });
            if (!req.form.pidsObj) {
                quantity = parseInt(req.form.quantity, 10);
                result = cartHelper.addProductToCart(
                    currentBasket,
                    productId,
                    quantity,
                    childProducts,
                    options
                );
            } else {
                // product set
                pidsObj = JSON.parse(req.form.pidsObj);
                result = {
                    error: false,
                    message: Resource.msg('text.alert.addedtobasket', 'product', null)
                };

                pidsObj.forEach(function (PIDObj) {
                    quantity = parseInt(PIDObj.qty, 10);
                    var pidOptions = PIDObj.options ? JSON.parse(PIDObj.options) : {};
                    var PIDObjResult = cartHelper.addProductToCart(
                        currentBasket,
                        PIDObj.pid,
                        quantity,
                        childProducts,
                        pidOptions
                    );
                    if (PIDObjResult.error) {
                        result.error = PIDObjResult.error;
                        result.message = PIDObjResult.message;
                    }
                });
            }
            if (!result.error) {
                cartHelper.ensureAllShipmentsHaveMethods(currentBasket);
                basketCalculationHelpers.calculateTotals(currentBasket);
            }
            currentBasket.custom.isBuynow = true;
            if (req.currentCustomer.raw.isAuthenticated()) {
                req.currentCustomer.raw.profile.custom.savedCart = JSON.stringify(savedCart);
            } else {
                currentBasket.custom.savedCart = JSON.stringify(savedCart);
            }
        });
    }

    // var urlObject = {
    //     url: URLUtils.url('Cart-ChooseBonusProducts').toString(),
    //     configureProductstUrl: URLUtils.url('Product-ShowBonusProducts').toString(),
    //     addToCartUrl: URLUtils.url('Cart-AddBonusProducts').toString()
    // };

    // var newBonusDiscountLineItem = cartHelper.getNewBonusDiscountLineItem(
    //     currentBasket,
    //     previousBonusDiscountLineItems,
    //     urlObject,
    //     result.uuid
    // );
    // if (newBonusDiscountLineItem) {
    //     var allLineItems = currentBasket.allProductLineItems;
    //     var collections = require('*/cartridge/scripts/util/collections');
    //     collections.forEach(allLineItems, function (pli) {
    //         if (pli.UUID === result.uuid) {
    //             Transaction.wrap(function () {
    //                 pli.custom.bonusProductLineItemUUID = 'bonus'; // eslint-disable-line no-param-reassign
    //                 pli.custom.preOrderUUID = pli.UUID; // eslint-disable-line no-param-reassign
    //             });
    //         }
    //     });
    // }

    var reportingURL = cartHelper.getReportingUrlAddToCart(currentBasket, result.error);
    res.json({
        reportingURL: reportingURL,
        message: result.message,
        error: result.error,
        pliUUID: result.uuid,
        redirectUrl: URLUtils.https('Checkout-Begin').toString()
    });

    next();
});

/**
 * BuyNow-Update : The BuyNow-Update endpoint is responsible for adding normal products to the cart after user place order/go to the previous pages after buynow checkout
 * @name Base/BuyNow-Update
 * @function
 * @memberof BuyNow
 * @param {httpparameter} - pid - product ID
 * @param {httpparameter} - quantity - quantity of product
 * @param {httpparameter} - options - list of product options
 * @param {returns} - json
 * @param {serverfunction} - post
 */
server.post('Update', function (req, res, next) {
    var BasketMgr = require('dw/order/BasketMgr');
    var Transaction = require('dw/system/Transaction');
    var Resource = require('dw/web/Resource');
    var buynowHelpers = require('*/cartridge/scripts/helpers/buynowHelpers');
    var ProductLineItemsModel = require('*/cartridge/models/productLineItems');
    var currentBasket = BasketMgr.getCurrentOrNewBasket();
    var refreshCart = false;

    if ((!currentBasket || !currentBasket.productLineItems.length) || currentBasket.custom.isBuynow) {
        Transaction.wrap(function () {
            currentBasket.productLineItems.toArray().forEach(function (pli) {
                currentBasket.removeProductLineItem(pli);
            });
            var basketCart = JSON.parse(('savedCart' in currentBasket.custom && currentBasket.custom.savedCart) || '{}');
            if (req.currentCustomer.raw.isAuthenticated()) {
                var savedCart = JSON.parse(req.currentCustomer.raw.profile.custom.savedCart || '{}');
                if (savedCart && Object.keys(savedCart).length) {
                    buynowHelpers.updateCart(savedCart);
                    refreshCart = true;
                } else if (basketCart && Object.keys(basketCart).length) {
                    buynowHelpers.updateCart(basketCart);
                    refreshCart = true;
                }
            } else if (basketCart && Object.keys(basketCart).length) {
                buynowHelpers.updateCart(basketCart);
                refreshCart = true;
            }
            currentBasket.custom.savedCart = null;
            currentBasket.custom.isBuynow = false;
        });
    }

    var quantityTotal = ProductLineItemsModel.getTotalQuantity(currentBasket.productLineItems);

    res.json({
        refreshCart: refreshCart,
        quantityTotal: quantityTotal,
        minicartCountOfItems: Resource.msgf('minicart.count', 'common', null, quantityTotal)
    });

    next();
});

module.exports = server.exports();
